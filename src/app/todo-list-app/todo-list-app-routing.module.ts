import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoListAppComponent } from "./todo-list-app.component";
import { TodoListComponent } from "./todo-list/todo-list.component";

const routes: Routes = [
  {
    path: '', component: TodoListAppComponent, children: [
      { path: 'list', component: TodoListComponent },
      { path: '', redirectTo: 'list', pathMatch: 'full' }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoListAppRoutingModule { }
