import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListAppComponent } from "./todo-list-app.component";
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoService } from '../utility-app/services/todo.service';
import { TodoListAppRoutingModule } from './todo-list-app-routing.module';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TodoListAppRoutingModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    TodoService,
    
  ],
  declarations: [TodoListAppComponent, TodoListComponent]
})
export class TodoListAppModule { }
